//[weather_shared](index.md)

# weather_shared

## Packages

| Name |
|---|
| [multi.platform.weather.shared](weather_shared/multi.platform.weather.shared/index.md) |
| [multi.platform.weather.shared.app.astronomy](weather_shared/multi.platform.weather.shared.app.astronomy/index.md) |
| [multi.platform.weather.shared.app.common](weather_shared/multi.platform.weather.shared.app.common/index.md) |
| [multi.platform.weather.shared.app.currentcondition](weather_shared/multi.platform.weather.shared.app.currentcondition/index.md) |
| [multi.platform.weather.shared.app.currenttemp](weather_shared/multi.platform.weather.shared.app.currenttemp/index.md) |
| [multi.platform.weather.shared.app.forecast](weather_shared/multi.platform.weather.shared.app.forecast/index.md) |
| [multi.platform.weather.shared.app.searchbar](weather_shared/multi.platform.weather.shared.app.searchbar/index.md) |
| [multi.platform.weather.shared.data.weather](weather_shared/multi.platform.weather.shared.data.weather/index.md) |
| [multi.platform.weather.shared.domain.weather](weather_shared/multi.platform.weather.shared.domain.weather/index.md) |
| [multi.platform.weather.shared.domain.weather.entity](weather_shared/multi.platform.weather.shared.domain.weather.entity/index.md) |
| [multi.platform.weather.shared.domain.weather.usecase](weather_shared/multi.platform.weather.shared.domain.weather.usecase/index.md) |
| [multi.platform.weather.shared.external](weather_shared/multi.platform.weather.shared.external/index.md) |
| [multi.platform.weather.shared.external.constants](weather_shared/multi.platform.weather.shared.external.constants/index.md) |
| [multi.platform.weather.shared.external.realm](weather_shared/multi.platform.weather.shared.external.realm/index.md) |
