//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.constants](../index.md)/[WeatherKey](index.md)

# WeatherKey

[common]\
object [WeatherKey](index.md)

## Properties

| Name | Summary |
|---|---|
| [NIGHT_MODE](-n-i-g-h-t_-m-o-d-e.md) | [common]<br>const val [NIGHT_MODE](-n-i-g-h-t_-m-o-d-e.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [QUERY](-q-u-e-r-y.md) | [common]<br>const val [QUERY](-q-u-e-r-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
