//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.constants](../index.md)/[WeatherKey](index.md)/[QUERY](-q-u-e-r-y.md)

# QUERY

[common]\
const val [QUERY](-q-u-e-r-y.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
