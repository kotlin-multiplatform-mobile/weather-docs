//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.constants](../index.md)/[WeatherKey](index.md)/[NIGHT_MODE](-n-i-g-h-t_-m-o-d-e.md)

# NIGHT_MODE

[common]\
const val [NIGHT_MODE](-n-i-g-h-t_-m-o-d-e.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
