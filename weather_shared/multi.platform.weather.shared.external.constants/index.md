//[weather_shared](../../index.md)/[multi.platform.weather.shared.external.constants](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherKey](-weather-key/index.md) | [common]<br>object [WeatherKey](-weather-key/index.md) |
