//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherLocationsLocalUseCase](index.md)/[call](call.md)

# call

[common]\
open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;
