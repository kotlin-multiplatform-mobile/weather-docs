//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherLocationsLocalUseCase](index.md)/[GetWeatherLocationsLocalUseCase](-get-weather-locations-local-use-case.md)

# GetWeatherLocationsLocalUseCase

[common]\
constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md))
