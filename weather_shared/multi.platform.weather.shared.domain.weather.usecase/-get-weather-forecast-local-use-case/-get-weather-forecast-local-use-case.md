//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherForecastLocalUseCase](index.md)/[GetWeatherForecastLocalUseCase](-get-weather-forecast-local-use-case.md)

# GetWeatherForecastLocalUseCase

[common]\
constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md))
