//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[SetWeatherLocationsLocalUseCase](index.md)/[call](call.md)

# call

[common]\
open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
