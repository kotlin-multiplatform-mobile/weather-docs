//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[SetWeatherLocationsLocalUseCase](index.md)

# SetWeatherLocationsLocalUseCase

[common]\
class [SetWeatherLocationsLocalUseCase](index.md)(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase

## Constructors

| | |
|---|---|
| [SetWeatherLocationsLocalUseCase](-set-weather-locations-local-use-case.md) | [common]<br>constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [call](call.md) | [common]<br>open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [onError](on-error.md) | [common]<br>open suspend override fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)) |
