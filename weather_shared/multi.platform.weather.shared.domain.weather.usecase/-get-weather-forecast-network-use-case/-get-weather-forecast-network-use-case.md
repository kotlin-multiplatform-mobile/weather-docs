//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherForecastNetworkUseCase](index.md)/[GetWeatherForecastNetworkUseCase](-get-weather-forecast-network-use-case.md)

# GetWeatherForecastNetworkUseCase

[common]\
constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md))
