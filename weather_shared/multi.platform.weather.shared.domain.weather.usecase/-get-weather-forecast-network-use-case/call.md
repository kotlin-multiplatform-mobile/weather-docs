//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherForecastNetworkUseCase](index.md)/[call](call.md)

# call

[common]\
open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
