//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[SetWeatherForecastLocalUseCase](index.md)/[SetWeatherForecastLocalUseCase](-set-weather-forecast-local-use-case.md)

# SetWeatherForecastLocalUseCase

[common]\
constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md))
