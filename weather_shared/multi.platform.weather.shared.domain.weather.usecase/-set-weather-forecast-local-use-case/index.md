//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[SetWeatherForecastLocalUseCase](index.md)

# SetWeatherForecastLocalUseCase

[common]\
class [SetWeatherForecastLocalUseCase](index.md)(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase

## Constructors

| | |
|---|---|
| [SetWeatherForecastLocalUseCase](-set-weather-forecast-local-use-case.md) | [common]<br>constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [call](call.md) | [common]<br>open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [onError](on-error.md) | [common]<br>open suspend override fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)) |
