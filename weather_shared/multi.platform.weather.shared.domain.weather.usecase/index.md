//[weather_shared](../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [GetWeatherForecastLocalUseCase](-get-weather-forecast-local-use-case/index.md) | [common]<br>class [GetWeatherForecastLocalUseCase](-get-weather-forecast-local-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
| [GetWeatherForecastNetworkUseCase](-get-weather-forecast-network-use-case/index.md) | [common]<br>class [GetWeatherForecastNetworkUseCase](-get-weather-forecast-network-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
| [GetWeatherLocationsLocalUseCase](-get-weather-locations-local-use-case/index.md) | [common]<br>class [GetWeatherLocationsLocalUseCase](-get-weather-locations-local-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
| [GetWeatherLocationsNetworkUseCase](-get-weather-locations-network-use-case/index.md) | [common]<br>class [GetWeatherLocationsNetworkUseCase](-get-weather-locations-network-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
| [SetWeatherForecastLocalUseCase](-set-weather-forecast-local-use-case/index.md) | [common]<br>class [SetWeatherForecastLocalUseCase](-set-weather-forecast-local-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
| [SetWeatherLocationsLocalUseCase](-set-weather-locations-local-use-case/index.md) | [common]<br>class [SetWeatherLocationsLocalUseCase](-set-weather-locations-local-use-case/index.md)(weatherRepository: [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase |
