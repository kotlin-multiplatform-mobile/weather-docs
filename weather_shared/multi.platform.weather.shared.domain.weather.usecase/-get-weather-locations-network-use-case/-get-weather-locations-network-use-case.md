//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherLocationsNetworkUseCase](index.md)/[GetWeatherLocationsNetworkUseCase](-get-weather-locations-network-use-case.md)

# GetWeatherLocationsNetworkUseCase

[common]\
constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md))
