//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.usecase](../index.md)/[GetWeatherLocationsNetworkUseCase](index.md)

# GetWeatherLocationsNetworkUseCase

[common]\
class [GetWeatherLocationsNetworkUseCase](index.md)(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) : CoreUseCase

## Constructors

| | |
|---|---|
| [GetWeatherLocationsNetworkUseCase](-get-weather-locations-network-use-case.md) | [common]<br>constructor(weatherRepository: [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [call](call.md) | [common]<br>open suspend override fun [call](call.md)(vararg args: [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt; |
| [onError](on-error.md) | [common]<br>open suspend override fun [onError](on-error.md)(e: [Exception](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-exception/index.html)) |
