//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.astronomy](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherAstronomyFragment](-weather-astronomy-fragment/index.md) | [android]<br>class [WeatherAstronomyFragment](-weather-astronomy-fragment/index.md) : CoreFragment, [WeatherAstronomyListener](-weather-astronomy-listener/index.md) |
| [WeatherAstronomyListener](-weather-astronomy-listener/index.md) | [android]<br>interface [WeatherAstronomyListener](-weather-astronomy-listener/index.md) |
