//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[Adapter](index.md)/[Adapter](-adapter.md)

# Adapter

[android]\
constructor(data: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherAstronomyFragment.ConditionItem](../-condition-item/index.md)&gt;)
