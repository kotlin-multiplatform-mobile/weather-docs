//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[Adapter](index.md)/[data](data.md)

# data

[android]\
val [data](data.md): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherAstronomyFragment.ConditionItem](../-condition-item/index.md)&gt;
