//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[Adapter](index.md)/[onCreateViewHolder](on-create-view-holder.md)

# onCreateViewHolder

[android]\
open override fun [onCreateViewHolder](on-create-view-holder.md)(parent: [ViewGroup](https://developer.android.com/reference/kotlin/android/view/ViewGroup.html), viewType: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [WeatherAstronomyFragment.Adapter.ViewHolder](-view-holder/index.md)
