//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[ConditionItem](index.md)/[ConditionItem](-condition-item.md)

# ConditionItem

[android]\
constructor(name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), time: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))
