//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[ConditionItem](index.md)/[name](name.md)

# name

[android]\
val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
