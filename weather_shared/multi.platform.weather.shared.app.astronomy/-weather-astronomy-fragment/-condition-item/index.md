//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[ConditionItem](index.md)

# ConditionItem

[android]\
inner class [ConditionItem](index.md)(val name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), val time: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))

## Constructors

| | |
|---|---|
| [ConditionItem](-condition-item.md) | [android]<br>constructor(name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), time: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) |

## Properties

| Name | Summary |
|---|---|
| [name](name.md) | [android]<br>val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [time](time.md) | [android]<br>val [time](time.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
