//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.astronomy](../../index.md)/[WeatherAstronomyFragment](../index.md)/[ConditionItem](index.md)/[time](time.md)

# time

[android]\
val [time](time.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
