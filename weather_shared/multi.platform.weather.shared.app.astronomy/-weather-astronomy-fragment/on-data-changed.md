//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.astronomy](../index.md)/[WeatherAstronomyFragment](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
open override fun [onDataChanged](on-data-changed.md)(weatherAstronomy: [WeatherAstronomy](../../multi.platform.weather.shared.domain.weather.entity/-weather-astronomy/index.md))
