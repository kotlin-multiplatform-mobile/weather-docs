//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.astronomy](../index.md)/[WeatherAstronomyListener](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
abstract fun [onDataChanged](on-data-changed.md)(weatherAstronomy: [WeatherAstronomy](../../multi.platform.weather.shared.domain.weather.entity/-weather-astronomy/index.md))
