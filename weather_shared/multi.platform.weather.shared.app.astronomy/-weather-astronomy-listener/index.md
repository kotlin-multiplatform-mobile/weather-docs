//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.astronomy](../index.md)/[WeatherAstronomyListener](index.md)

# WeatherAstronomyListener

interface [WeatherAstronomyListener](index.md)

#### Inheritors

| |
|---|
| [WeatherAstronomyFragment](../-weather-astronomy-fragment/index.md) |

## Functions

| Name | Summary |
|---|---|
| [onDataChanged](on-data-changed.md) | [android]<br>abstract fun [onDataChanged](on-data-changed.md)(weatherAstronomy: [WeatherAstronomy](../../multi.platform.weather.shared.domain.weather.entity/-weather-astronomy/index.md)) |
