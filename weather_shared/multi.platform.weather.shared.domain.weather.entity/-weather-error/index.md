//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherError](index.md)

# WeatherError

[common]\
@Serializable

data class [WeatherError](index.md)(val error: [WeatherError.Error](-error/index.md))

## Constructors

| | |
|---|---|
| [WeatherError](-weather-error.md) | [common]<br>constructor(error: [WeatherError.Error](-error/index.md)) |

## Types

| Name | Summary |
|---|---|
| [Error](-error/index.md) | [common]<br>@Serializable<br>data class [Error](-error/index.md)(val code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, val message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?) |

## Properties

| Name | Summary |
|---|---|
| [error](error.md) | [common]<br>val [error](error.md): [WeatherError.Error](-error/index.md) |
