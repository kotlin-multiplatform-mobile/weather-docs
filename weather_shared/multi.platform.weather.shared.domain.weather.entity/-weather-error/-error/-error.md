//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../../index.md)/[WeatherError](../index.md)/[Error](index.md)/[Error](-error.md)

# Error

[common]\
constructor(code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)
