//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../../index.md)/[WeatherError](../index.md)/[Error](index.md)/[message](message.md)

# message

[common]\
val [message](message.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
