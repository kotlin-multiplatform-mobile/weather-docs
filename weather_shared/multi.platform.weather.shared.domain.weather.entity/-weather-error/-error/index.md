//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../../index.md)/[WeatherError](../index.md)/[Error](index.md)

# Error

[common]\
@Serializable

data class [Error](index.md)(val code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, val message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)

## Constructors

| | |
|---|---|
| [Error](-error.md) | [common]<br>constructor(code: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?) |

## Properties

| Name | Summary |
|---|---|
| [code](code.md) | [common]<br>val [code](code.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [message](message.md) | [common]<br>val [message](message.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
