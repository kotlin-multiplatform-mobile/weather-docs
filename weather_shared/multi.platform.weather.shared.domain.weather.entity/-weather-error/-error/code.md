//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../../index.md)/[WeatherError](../index.md)/[Error](index.md)/[code](code.md)

# code

[common]\
val [code](code.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
