//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherError](index.md)/[WeatherError](-weather-error.md)

# WeatherError

[common]\
constructor(error: [WeatherError.Error](-error/index.md))
