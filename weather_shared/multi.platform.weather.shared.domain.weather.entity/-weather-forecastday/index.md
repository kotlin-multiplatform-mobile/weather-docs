//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecastday](index.md)

# WeatherForecastday

[common]\
@Serializable

open class [WeatherForecastday](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherForecastday](-weather-forecastday.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [astro](astro.md) | [common]<br>var [astro](astro.md): [WeatherAstronomy](../-weather-astronomy/index.md)? |
| [date](date.md) | [common]<br>var [date](date.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [dateEpoch](date-epoch.md) | [common]<br>var [dateEpoch](date-epoch.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [day](day.md) | [common]<br>var [day](day.md): [Weatherday](../-weatherday/index.md)? |
| [hour](hour.md) | [common]<br>@Serializable(with = [RealmListWeatherSerializer::class](../../multi.platform.weather.shared.external.realm/-realm-list-weather-serializer/index.md))<br>var [hour](hour.md): RealmList&lt;[Weather](../-weather/index.md)&gt;? |
