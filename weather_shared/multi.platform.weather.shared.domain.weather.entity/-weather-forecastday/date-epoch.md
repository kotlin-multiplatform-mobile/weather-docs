//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecastday](index.md)/[dateEpoch](date-epoch.md)

# dateEpoch

[common]\
var [dateEpoch](date-epoch.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
