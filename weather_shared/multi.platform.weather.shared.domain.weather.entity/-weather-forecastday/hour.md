//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecastday](index.md)/[hour](hour.md)

# hour

[common]\

@Serializable(with = [RealmListWeatherSerializer::class](../../multi.platform.weather.shared.external.realm/-realm-list-weather-serializer/index.md))

var [hour](hour.md): RealmList&lt;[Weather](../-weather/index.md)&gt;?
