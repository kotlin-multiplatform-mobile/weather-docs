//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecast](index.md)

# WeatherForecast

[common]\
@Serializable

open class [WeatherForecast](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherForecast](-weather-forecast.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [current](current.md) | [common]<br>var [current](current.md): [Weather](../-weather/index.md)? |
| [forecast](forecast.md) | [common]<br>var [forecast](forecast.md): [WeatherForecastDetail](../-weather-forecast-detail/index.md)? |
| [location](location.md) | [common]<br>var [location](location.md): [WeatherLocation](../-weather-location/index.md)? |
