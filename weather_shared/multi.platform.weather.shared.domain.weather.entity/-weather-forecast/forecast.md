//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecast](index.md)/[forecast](forecast.md)

# forecast

[common]\
var [forecast](forecast.md): [WeatherForecastDetail](../-weather-forecast-detail/index.md)?
