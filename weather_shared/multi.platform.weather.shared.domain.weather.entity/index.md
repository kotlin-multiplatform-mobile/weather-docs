//[weather_shared](../../index.md)/[multi.platform.weather.shared.domain.weather.entity](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [Weather](-weather/index.md) | [common]<br>@Serializable<br>open class [Weather](-weather/index.md) : RealmObject |
| [WeatherAstronomy](-weather-astronomy/index.md) | [common]<br>@Serializable<br>open class [WeatherAstronomy](-weather-astronomy/index.md) : RealmObject |
| [WeatherCondition](-weather-condition/index.md) | [common]<br>@Serializable<br>open class [WeatherCondition](-weather-condition/index.md) : RealmObject |
| [Weatherday](-weatherday/index.md) | [common]<br>@Serializable<br>open class [Weatherday](-weatherday/index.md) : RealmObject |
| [WeatherError](-weather-error/index.md) | [common]<br>@Serializable<br>data class [WeatherError](-weather-error/index.md)(val error: [WeatherError.Error](-weather-error/-error/index.md)) |
| [WeatherForecast](-weather-forecast/index.md) | [common]<br>@Serializable<br>open class [WeatherForecast](-weather-forecast/index.md) : RealmObject |
| [WeatherForecastday](-weather-forecastday/index.md) | [common]<br>@Serializable<br>open class [WeatherForecastday](-weather-forecastday/index.md) : RealmObject |
| [WeatherForecastDetail](-weather-forecast-detail/index.md) | [common]<br>@Serializable<br>open class [WeatherForecastDetail](-weather-forecast-detail/index.md) : RealmObject |
| [WeatherLocation](-weather-location/index.md) | [common]<br>@Serializable<br>open class [WeatherLocation](-weather-location/index.md) : RealmObject |
