//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherCondition](index.md)

# WeatherCondition

[common]\
@Serializable

open class [WeatherCondition](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherCondition](-weather-condition.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [code](code.md) | [common]<br>var [code](code.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [icon](icon.md) | [common]<br>var [icon](icon.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [text](text.md) | [common]<br>var [text](text.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
