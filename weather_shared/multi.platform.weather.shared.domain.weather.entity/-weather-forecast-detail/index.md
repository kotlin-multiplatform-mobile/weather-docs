//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecastDetail](index.md)

# WeatherForecastDetail

[common]\
@Serializable

open class [WeatherForecastDetail](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherForecastDetail](-weather-forecast-detail.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [forecastday](forecastday.md) | [common]<br>@Serializable(with = [RealmListWeatherForecastdaySerializer::class](../../multi.platform.weather.shared.external.realm/-realm-list-weather-forecastday-serializer/index.md))<br>var [forecastday](forecastday.md): RealmList&lt;[WeatherForecastday](../-weather-forecastday/index.md)&gt;? |
