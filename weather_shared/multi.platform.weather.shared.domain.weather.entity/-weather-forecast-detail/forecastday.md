//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherForecastDetail](index.md)/[forecastday](forecastday.md)

# forecastday

[common]\

@Serializable(with = [RealmListWeatherForecastdaySerializer::class](../../multi.platform.weather.shared.external.realm/-realm-list-weather-forecastday-serializer/index.md))

var [forecastday](forecastday.md): RealmList&lt;[WeatherForecastday](../-weather-forecastday/index.md)&gt;?
