//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherLocation](index.md)

# WeatherLocation

[common]\
@Serializable

open class [WeatherLocation](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherLocation](-weather-location.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [country](country.md) | [common]<br>var [country](country.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [id](id.md) | [common]<br>var [id](id.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [lat](lat.md) | [common]<br>var [lat](lat.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [localtime](localtime.md) | [common]<br>var [localtime](localtime.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [localtimeEpoch](localtime-epoch.md) | [common]<br>var [localtimeEpoch](localtime-epoch.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [lon](lon.md) | [common]<br>var [lon](lon.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [name](name.md) | [common]<br>var [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [region](region.md) | [common]<br>var [region](region.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [tzId](tz-id.md) | [common]<br>var [tzId](tz-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [url](url.md) | [common]<br>var [url](url.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
