//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherLocation](index.md)/[localtimeEpoch](localtime-epoch.md)

# localtimeEpoch

[common]\
var [localtimeEpoch](localtime-epoch.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
