//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherAstronomy](index.md)

# WeatherAstronomy

[common]\
@Serializable

open class [WeatherAstronomy](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [WeatherAstronomy](-weather-astronomy.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [isMoonUp](is-moon-up.md) | [common]<br>var [isMoonUp](is-moon-up.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [isSunUp](is-sun-up.md) | [common]<br>var [isSunUp](is-sun-up.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [moonIllumination](moon-illumination.md) | [common]<br>var [moonIllumination](moon-illumination.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [moonPhase](moon-phase.md) | [common]<br>var [moonPhase](moon-phase.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [moonrise](moonrise.md) | [common]<br>var [moonrise](moonrise.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [moonset](moonset.md) | [common]<br>var [moonset](moonset.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [sunrise](sunrise.md) | [common]<br>var [sunrise](sunrise.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [sunset](sunset.md) | [common]<br>var [sunset](sunset.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
