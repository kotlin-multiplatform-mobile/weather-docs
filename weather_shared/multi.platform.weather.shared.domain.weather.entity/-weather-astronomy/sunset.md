//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherAstronomy](index.md)/[sunset](sunset.md)

# sunset

[common]\
var [sunset](sunset.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
