//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherAstronomy](index.md)/[moonset](moonset.md)

# moonset

[common]\
var [moonset](moonset.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
