//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherAstronomy](index.md)/[sunrise](sunrise.md)

# sunrise

[common]\
var [sunrise](sunrise.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
