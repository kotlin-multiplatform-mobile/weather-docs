//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[WeatherAstronomy](index.md)/[isMoonUp](is-moon-up.md)

# isMoonUp

[common]\
var [isMoonUp](is-moon-up.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)?
