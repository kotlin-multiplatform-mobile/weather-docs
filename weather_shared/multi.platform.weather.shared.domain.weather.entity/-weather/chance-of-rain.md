//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[chanceOfRain](chance-of-rain.md)

# chanceOfRain

[common]\
var [chanceOfRain](chance-of-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
