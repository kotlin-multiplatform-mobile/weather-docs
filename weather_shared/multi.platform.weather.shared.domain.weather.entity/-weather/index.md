//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)

# Weather

[common]\
@Serializable

open class [Weather](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [Weather](-weather.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [chanceOfRain](chance-of-rain.md) | [common]<br>var [chanceOfRain](chance-of-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [chanceOfSnow](chance-of-snow.md) | [common]<br>var [chanceOfSnow](chance-of-snow.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [cloud](cloud.md) | [common]<br>var [cloud](cloud.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [condition](condition.md) | [common]<br>var [condition](condition.md): [WeatherCondition](../-weather-condition/index.md)? |
| [dewpointC](dewpoint-c.md) | [common]<br>var [dewpointC](dewpoint-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [dewpointF](dewpoint-f.md) | [common]<br>var [dewpointF](dewpoint-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [feelslikeC](feelslike-c.md) | [common]<br>var [feelslikeC](feelslike-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [feelslikeF](feelslike-f.md) | [common]<br>var [feelslikeF](feelslike-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [gustKph](gust-kph.md) | [common]<br>var [gustKph](gust-kph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [gustMph](gust-mph.md) | [common]<br>var [gustMph](gust-mph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [heatindexC](heatindex-c.md) | [common]<br>var [heatindexC](heatindex-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [heatindexF](heatindex-f.md) | [common]<br>var [heatindexF](heatindex-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [humidity](humidity.md) | [common]<br>var [humidity](humidity.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [isDay](is-day.md) | [common]<br>var [isDay](is-day.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [precipIn](precip-in.md) | [common]<br>var [precipIn](precip-in.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [precipMm](precip-mm.md) | [common]<br>var [precipMm](precip-mm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [pressureIn](pressure-in.md) | [common]<br>var [pressureIn](pressure-in.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [pressureMb](pressure-mb.md) | [common]<br>var [pressureMb](pressure-mb.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [tempC](temp-c.md) | [common]<br>var [tempC](temp-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [tempF](temp-f.md) | [common]<br>var [tempF](temp-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [time](time.md) | [common]<br>var [time](time.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [timeEpoch](time-epoch.md) | [common]<br>var [timeEpoch](time-epoch.md): [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html)? |
| [uv](uv.md) | [common]<br>var [uv](uv.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [visKm](vis-km.md) | [common]<br>var [visKm](vis-km.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [visMiles](vis-miles.md) | [common]<br>var [visMiles](vis-miles.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [willItRain](will-it-rain.md) | [common]<br>var [willItRain](will-it-rain.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [willItSnow](will-it-snow.md) | [common]<br>var [willItSnow](will-it-snow.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)? |
| [windchillC](windchill-c.md) | [common]<br>var [windchillC](windchill-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [windchillF](windchill-f.md) | [common]<br>var [windchillF](windchill-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [windDegree](wind-degree.md) | [common]<br>var [windDegree](wind-degree.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [windDir](wind-dir.md) | [common]<br>var [windDir](wind-dir.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [windKph](wind-kph.md) | [common]<br>var [windKph](wind-kph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
| [windMph](wind-mph.md) | [common]<br>var [windMph](wind-mph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html) |
