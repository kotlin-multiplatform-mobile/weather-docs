//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[willItRain](will-it-rain.md)

# willItRain

[common]\
var [willItRain](will-it-rain.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)?
