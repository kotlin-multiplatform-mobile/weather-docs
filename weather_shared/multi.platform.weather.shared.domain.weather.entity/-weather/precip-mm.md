//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[precipMm](precip-mm.md)

# precipMm

[common]\
var [precipMm](precip-mm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)
