//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[windDir](wind-dir.md)

# windDir

[common]\
var [windDir](wind-dir.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
