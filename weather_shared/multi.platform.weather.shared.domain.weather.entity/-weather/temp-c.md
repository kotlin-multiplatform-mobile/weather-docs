//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[tempC](temp-c.md)

# tempC

[common]\
var [tempC](temp-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)
