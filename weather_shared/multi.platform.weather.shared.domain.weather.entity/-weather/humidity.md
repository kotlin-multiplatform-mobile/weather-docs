//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[humidity](humidity.md)

# humidity

[common]\
var [humidity](humidity.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)
