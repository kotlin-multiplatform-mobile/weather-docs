//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weather](index.md)/[chanceOfSnow](chance-of-snow.md)

# chanceOfSnow

[common]\
var [chanceOfSnow](chance-of-snow.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)?
