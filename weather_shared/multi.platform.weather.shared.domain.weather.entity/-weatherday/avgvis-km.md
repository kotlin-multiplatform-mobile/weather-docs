//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[avgvisKm](avgvis-km.md)

# avgvisKm

[common]\
var [avgvisKm](avgvis-km.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
