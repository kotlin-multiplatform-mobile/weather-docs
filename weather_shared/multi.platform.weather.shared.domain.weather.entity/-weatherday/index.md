//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)

# Weatherday

[common]\
@Serializable

open class [Weatherday](index.md) : RealmObject

## Constructors

| | |
|---|---|
| [Weatherday](-weatherday.md) | [common]<br>constructor() |

## Properties

| Name | Summary |
|---|---|
| [avghumidity](avghumidity.md) | [common]<br>var [avghumidity](avghumidity.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [avgtempC](avgtemp-c.md) | [common]<br>var [avgtempC](avgtemp-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [avgtempF](avgtemp-f.md) | [common]<br>var [avgtempF](avgtemp-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [avgvisKm](avgvis-km.md) | [common]<br>var [avgvisKm](avgvis-km.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [avgvisMiles](avgvis-miles.md) | [common]<br>var [avgvisMiles](avgvis-miles.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [condition](condition.md) | [common]<br>var [condition](condition.md): [WeatherCondition](../-weather-condition/index.md)? |
| [dailyChanceOfRain](daily-chance-of-rain.md) | [common]<br>var [dailyChanceOfRain](daily-chance-of-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [dailyChanceOfSnow](daily-chance-of-snow.md) | [common]<br>var [dailyChanceOfSnow](daily-chance-of-snow.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [dailyWillItRain](daily-will-it-rain.md) | [common]<br>var [dailyWillItRain](daily-will-it-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [dailyWillItSnow](daily-will-it-snow.md) | [common]<br>var [dailyWillItSnow](daily-will-it-snow.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [maxtempC](maxtemp-c.md) | [common]<br>var [maxtempC](maxtemp-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [maxtempF](maxtemp-f.md) | [common]<br>var [maxtempF](maxtemp-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [maxwindKph](maxwind-kph.md) | [common]<br>var [maxwindKph](maxwind-kph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [maxwindMph](maxwind-mph.md) | [common]<br>var [maxwindMph](maxwind-mph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [mintempC](mintemp-c.md) | [common]<br>var [mintempC](mintemp-c.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [mintempF](mintemp-f.md) | [common]<br>var [mintempF](mintemp-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [totalprecipIn](totalprecip-in.md) | [common]<br>var [totalprecipIn](totalprecip-in.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [totalprecipMm](totalprecip-mm.md) | [common]<br>var [totalprecipMm](totalprecip-mm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [totalsnowCm](totalsnow-cm.md) | [common]<br>var [totalsnowCm](totalsnow-cm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
| [uv](uv.md) | [common]<br>var [uv](uv.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)? |
