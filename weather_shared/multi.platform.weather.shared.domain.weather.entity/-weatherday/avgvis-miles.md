//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[avgvisMiles](avgvis-miles.md)

# avgvisMiles

[common]\
var [avgvisMiles](avgvis-miles.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
