//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[dailyChanceOfRain](daily-chance-of-rain.md)

# dailyChanceOfRain

[common]\
var [dailyChanceOfRain](daily-chance-of-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
