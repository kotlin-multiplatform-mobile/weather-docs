//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[maxwindMph](maxwind-mph.md)

# maxwindMph

[common]\
var [maxwindMph](maxwind-mph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
