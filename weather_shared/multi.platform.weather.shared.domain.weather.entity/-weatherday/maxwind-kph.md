//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[maxwindKph](maxwind-kph.md)

# maxwindKph

[common]\
var [maxwindKph](maxwind-kph.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
