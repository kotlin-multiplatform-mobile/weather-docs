//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[totalsnowCm](totalsnow-cm.md)

# totalsnowCm

[common]\
var [totalsnowCm](totalsnow-cm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
