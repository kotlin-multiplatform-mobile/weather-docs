//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[totalprecipIn](totalprecip-in.md)

# totalprecipIn

[common]\
var [totalprecipIn](totalprecip-in.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
