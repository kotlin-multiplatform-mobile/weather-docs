//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[dailyWillItRain](daily-will-it-rain.md)

# dailyWillItRain

[common]\
var [dailyWillItRain](daily-will-it-rain.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
