//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[dailyChanceOfSnow](daily-chance-of-snow.md)

# dailyChanceOfSnow

[common]\
var [dailyChanceOfSnow](daily-chance-of-snow.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
