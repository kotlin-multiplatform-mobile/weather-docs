//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[dailyWillItSnow](daily-will-it-snow.md)

# dailyWillItSnow

[common]\
var [dailyWillItSnow](daily-will-it-snow.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
