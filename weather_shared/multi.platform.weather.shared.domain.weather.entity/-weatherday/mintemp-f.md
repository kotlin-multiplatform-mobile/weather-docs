//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[mintempF](mintemp-f.md)

# mintempF

[common]\
var [mintempF](mintemp-f.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
