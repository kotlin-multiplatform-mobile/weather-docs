//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather.entity](../index.md)/[Weatherday](index.md)/[totalprecipMm](totalprecip-mm.md)

# totalprecipMm

[common]\
var [totalprecipMm](totalprecip-mm.md): [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html)?
