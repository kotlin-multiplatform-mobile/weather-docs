//[weather_shared](../../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../../index.md)/[WeatherForecastFragment](../../index.md)/[AdapterForecast](../index.md)/[ViewHolder](index.md)/[bind](bind.md)

# bind

[android]\
fun [bind](bind.md)(weather: [Weather](../../../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md))
