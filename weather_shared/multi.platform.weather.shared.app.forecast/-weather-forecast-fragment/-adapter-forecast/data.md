//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[AdapterForecast](index.md)/[data](data.md)

# data

[android]\
val [data](data.md): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[Weather](../../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;
