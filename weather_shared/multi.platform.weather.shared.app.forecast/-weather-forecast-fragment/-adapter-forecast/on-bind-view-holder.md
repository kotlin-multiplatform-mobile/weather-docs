//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[AdapterForecast](index.md)/[onBindViewHolder](on-bind-view-holder.md)

# onBindViewHolder

[android]\
open override fun [onBindViewHolder](on-bind-view-holder.md)(holder: [WeatherForecastFragment.AdapterForecast.ViewHolder](-view-holder/index.md), position: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html))
