//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[AdapterForecast](index.md)/[AdapterForecast](-adapter-forecast.md)

# AdapterForecast

[android]\
constructor(data: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[Weather](../../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;)
