//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[ConditionItem](index.md)/[ConditionItem](-condition-item.md)

# ConditionItem

[android]\
constructor(name: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), condition: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))
