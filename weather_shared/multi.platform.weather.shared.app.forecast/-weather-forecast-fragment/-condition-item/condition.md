//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[ConditionItem](index.md)/[condition](condition.md)

# condition

[android]\
val [condition](condition.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
