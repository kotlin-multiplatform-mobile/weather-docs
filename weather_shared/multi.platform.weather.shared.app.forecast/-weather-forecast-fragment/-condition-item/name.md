//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[ConditionItem](index.md)/[name](name.md)

# name

[android]\
val [name](name.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
