//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.forecast](../index.md)/[WeatherForecastFragment](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
open override fun [onDataChanged](on-data-changed.md)(weathers: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;)
