//[weather_shared](../../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../../index.md)/[WeatherForecastFragment](../../index.md)/[AdapterCondition](../index.md)/[ViewHolder](index.md)

# ViewHolder

[android]\
inner class [ViewHolder](index.md)(view: [View](https://developer.android.com/reference/kotlin/android/view/View.html), val itemBinding: &lt;Error class: unknown class&gt;) : [RecyclerView.ViewHolder](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/RecyclerView.ViewHolder.html)

## Constructors

| | |
|---|---|
| [ViewHolder](-view-holder.md) | [android]<br>constructor(view: [View](https://developer.android.com/reference/kotlin/android/view/View.html), itemBinding: &lt;Error class: unknown class&gt;) |

## Properties

| Name | Summary |
|---|---|
| [itemBinding](item-binding.md) | [android]<br>val [itemBinding](item-binding.md): &lt;Error class: unknown class&gt; |
| [itemView](index.md#29975211%2FProperties%2F-538093729) | [android]<br>@[NonNull](https://developer.android.com/reference/kotlin/androidx/annotation/NonNull.html)<br>val [itemView](index.md#29975211%2FProperties%2F-538093729): [View](https://developer.android.com/reference/kotlin/android/view/View.html) |

## Functions

| Name | Summary |
|---|---|
| [bind](bind.md) | [android]<br>fun [bind](bind.md)(conditionItem: [WeatherForecastFragment.ConditionItem](../../-condition-item/index.md)) |
| [getAdapterPosition](index.md#644519777%2FFunctions%2F-538093729) | [android]<br>fun [getAdapterPosition](index.md#644519777%2FFunctions%2F-538093729)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getItemId](index.md#1378485811%2FFunctions%2F-538093729) | [android]<br>fun [getItemId](index.md#1378485811%2FFunctions%2F-538093729)(): [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) |
| [getItemViewType](index.md#-1649344625%2FFunctions%2F-538093729) | [android]<br>fun [getItemViewType](index.md#-1649344625%2FFunctions%2F-538093729)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getLayoutPosition](index.md#-1407255826%2FFunctions%2F-538093729) | [android]<br>fun [getLayoutPosition](index.md#-1407255826%2FFunctions%2F-538093729)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getOldPosition](index.md#-1203059319%2FFunctions%2F-538093729) | [android]<br>fun [getOldPosition](index.md#-1203059319%2FFunctions%2F-538093729)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [getPosition](index.md#-1155470344%2FFunctions%2F-538093729) | [android]<br>fun [~~getPosition~~](index.md#-1155470344%2FFunctions%2F-538093729)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [isRecyclable](index.md#-1703443315%2FFunctions%2F-538093729) | [android]<br>fun [isRecyclable](index.md#-1703443315%2FFunctions%2F-538093729)(): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
| [setIsRecyclable](index.md#-1860912636%2FFunctions%2F-538093729) | [android]<br>fun [setIsRecyclable](index.md#-1860912636%2FFunctions%2F-538093729)(p0: [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)) |
| [toString](index.md#-1200015593%2FFunctions%2F-538093729) | [android]<br>open override fun [toString](index.md#-1200015593%2FFunctions%2F-538093729)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
