//[weather_shared](../../../../index.md)/[multi.platform.weather.shared.app.forecast](../../index.md)/[WeatherForecastFragment](../index.md)/[AdapterCondition](index.md)/[getItemCount](get-item-count.md)

# getItemCount

[android]\
open override fun [getItemCount](get-item-count.md)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
