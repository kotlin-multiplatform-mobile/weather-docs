//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.forecast](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherForecastFragment](-weather-forecast-fragment/index.md) | [android]<br>class [WeatherForecastFragment](-weather-forecast-fragment/index.md) : CoreFragment, [WeatherForecastListener](-weather-forecast-listener/index.md) |
| [WeatherForecastListener](-weather-forecast-listener/index.md) | [android]<br>interface [WeatherForecastListener](-weather-forecast-listener/index.md) |
