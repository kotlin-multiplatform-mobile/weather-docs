//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.forecast](../index.md)/[WeatherForecastListener](index.md)

# WeatherForecastListener

interface [WeatherForecastListener](index.md)

#### Inheritors

| |
|---|
| [WeatherForecastFragment](../-weather-forecast-fragment/index.md) |

## Functions

| Name | Summary |
|---|---|
| [onDataChanged](on-data-changed.md) | [android]<br>abstract fun [onDataChanged](on-data-changed.md)(weathers: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;) |
