//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.currentcondition](../index.md)/[WeatherCurrentConditionListener](index.md)

# WeatherCurrentConditionListener

interface [WeatherCurrentConditionListener](index.md)

#### Inheritors

| |
|---|
| [WeatherCurrentConditionFragment](../-weather-current-condition-fragment/index.md) |

## Functions

| Name | Summary |
|---|---|
| [onDataChanged](on-data-changed.md) | [android]<br>abstract fun [onDataChanged](on-data-changed.md)(weatherCondition: [WeatherCondition](../../multi.platform.weather.shared.domain.weather.entity/-weather-condition/index.md)) |
