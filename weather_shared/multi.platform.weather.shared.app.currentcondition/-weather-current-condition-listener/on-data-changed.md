//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.currentcondition](../index.md)/[WeatherCurrentConditionListener](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
abstract fun [onDataChanged](on-data-changed.md)(weatherCondition: [WeatherCondition](../../multi.platform.weather.shared.domain.weather.entity/-weather-condition/index.md))
