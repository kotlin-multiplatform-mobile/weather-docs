//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.currentcondition](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherCurrentConditionFragment](-weather-current-condition-fragment/index.md) | [android]<br>class [WeatherCurrentConditionFragment](-weather-current-condition-fragment/index.md) : CoreFragment, [WeatherCurrentConditionListener](-weather-current-condition-listener/index.md) |
| [WeatherCurrentConditionListener](-weather-current-condition-listener/index.md) | [android]<br>interface [WeatherCurrentConditionListener](-weather-current-condition-listener/index.md) |
