//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.currentcondition](../index.md)/[WeatherCurrentConditionFragment](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
open override fun [onDataChanged](on-data-changed.md)(weatherCondition: [WeatherCondition](../../multi.platform.weather.shared.domain.weather.entity/-weather-condition/index.md))
