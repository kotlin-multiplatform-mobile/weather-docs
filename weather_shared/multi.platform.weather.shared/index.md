//[weather_shared](../../index.md)/[multi.platform.weather.shared](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherModule](-weather-module/index.md) | [common]<br>class [WeatherModule](-weather-module/index.md)(weatherConfig: [WeatherConfig](../multi.platform.weather.shared.external/-weather-config/index.md)) |

## Functions

| Name | Summary |
|---|---|
| [weatherModule](weather-module.md) | [common, android, ios]<br>[common]<br>expect fun [weatherModule](weather-module.md)(): Module<br>[android, ios]<br>actual fun [weatherModule](weather-module.md)(): Module |
