//[weather_shared](../../../index.md)/[multi.platform.weather.shared](../index.md)/[WeatherModule](index.md)

# WeatherModule

[common]\
class [WeatherModule](index.md)(weatherConfig: [WeatherConfig](../../multi.platform.weather.shared.external/-weather-config/index.md))

## Constructors

| | |
|---|---|
| [WeatherModule](-weather-module.md) | [common]<br>constructor(weatherConfig: [WeatherConfig](../../multi.platform.weather.shared.external/-weather-config/index.md)) |

## Properties

| Name | Summary |
|---|---|
| [realmConfig](realm-config.md) | [common]<br>val [realmConfig](realm-config.md): RealmConfiguration |

## Functions

| Name | Summary |
|---|---|
| [invoke](invoke.md) | [common]<br>operator fun [invoke](invoke.md)(): Module |
| [provideDB](provide-d-b.md) | [common]<br>fun [provideDB](provide-d-b.md)(): Realm |
