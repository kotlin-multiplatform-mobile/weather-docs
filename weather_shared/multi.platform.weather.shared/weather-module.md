//[weather_shared](../../index.md)/[multi.platform.weather.shared](index.md)/[weatherModule](weather-module.md)

# weatherModule

[common, android, ios]\
[common]\
expect fun [weatherModule](weather-module.md)(): Module

[android, ios]\
actual fun [weatherModule](weather-module.md)(): Module
