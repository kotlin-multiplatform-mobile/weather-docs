//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.searchbar](../index.md)/[WeatherSearchbarFragment](index.md)/[onViewCreated](on-view-created.md)

# onViewCreated

[android]\
open override fun [onViewCreated](on-view-created.md)(view: [View](https://developer.android.com/reference/kotlin/android/view/View.html), savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?)
