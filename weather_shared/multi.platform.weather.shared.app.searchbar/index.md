//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.searchbar](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ListWeatherLocationViewModel](-list-weather-location-view-model/index.md) | [common]<br>class [ListWeatherLocationViewModel](-list-weather-location-view-model/index.md)(getUseCase: [GetWeatherLocationsNetworkUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-network-use-case/index.md), readUseCase: [GetWeatherLocationsLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-local-use-case/index.md), setUseCase: [SetWeatherLocationsLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-set-weather-locations-local-use-case/index.md)) : ListViewModel&lt;[WeatherLocation](../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md), [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;, [GetWeatherLocationsNetworkUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-network-use-case/index.md), [GetWeatherLocationsLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-local-use-case/index.md), [SetWeatherLocationsLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-set-weather-locations-local-use-case/index.md)&gt; |
| [WeatherSearchbarFragment](-weather-searchbar-fragment/index.md) | [android]<br>class [WeatherSearchbarFragment](-weather-searchbar-fragment/index.md) : CoreFragment, [WeatherSearchbarListener](-weather-searchbar-listener/index.md) |
| [WeatherSearchbarListener](-weather-searchbar-listener/index.md) | [android]<br>interface [WeatherSearchbarListener](-weather-searchbar-listener/index.md) |
