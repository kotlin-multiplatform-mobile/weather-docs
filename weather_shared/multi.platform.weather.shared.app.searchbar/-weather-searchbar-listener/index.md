//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.searchbar](../index.md)/[WeatherSearchbarListener](index.md)

# WeatherSearchbarListener

interface [WeatherSearchbarListener](index.md)

#### Inheritors

| |
|---|
| [WeatherSearchbarFragment](../-weather-searchbar-fragment/index.md) |

## Functions

| Name | Summary |
|---|---|
| [onWeatherSearch](on-weather-search.md) | [android]<br>open fun [onWeatherSearch](on-weather-search.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)) |
| [refreshWeatherSearch](refresh-weather-search.md) | [android]<br>open fun [refreshWeatherSearch](refresh-weather-search.md)() |
