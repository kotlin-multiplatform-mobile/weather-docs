//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.searchbar](../index.md)/[WeatherSearchbarListener](index.md)/[onWeatherSearch](on-weather-search.md)

# onWeatherSearch

[android]\
open fun [onWeatherSearch](on-weather-search.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))
