//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.searchbar](../index.md)/[ListWeatherLocationViewModel](index.md)/[ListWeatherLocationViewModel](-list-weather-location-view-model.md)

# ListWeatherLocationViewModel

[common]\
constructor(getUseCase: [GetWeatherLocationsNetworkUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-network-use-case/index.md), readUseCase: [GetWeatherLocationsLocalUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-get-weather-locations-local-use-case/index.md), setUseCase: [SetWeatherLocationsLocalUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-set-weather-locations-local-use-case/index.md))
