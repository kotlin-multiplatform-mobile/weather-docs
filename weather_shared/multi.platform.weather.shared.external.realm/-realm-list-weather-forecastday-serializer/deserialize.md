//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherForecastdaySerializer](index.md)/[deserialize](deserialize.md)

# deserialize

[common]\
open override fun [deserialize](deserialize.md)(decoder: Decoder): RealmList&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;
