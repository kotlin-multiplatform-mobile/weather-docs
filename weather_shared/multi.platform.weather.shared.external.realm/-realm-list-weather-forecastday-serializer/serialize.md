//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherForecastdaySerializer](index.md)/[serialize](serialize.md)

# serialize

[common]\
open override fun [serialize](serialize.md)(encoder: Encoder, value: RealmList&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;)
