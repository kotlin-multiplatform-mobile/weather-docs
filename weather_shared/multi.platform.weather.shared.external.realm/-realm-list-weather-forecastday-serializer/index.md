//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherForecastdaySerializer](index.md)

# RealmListWeatherForecastdaySerializer

[common]\
class [RealmListWeatherForecastdaySerializer](index.md)(dataSerializer: KSerializer&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;) : KSerializer&lt;RealmList&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;&gt;

## Constructors

| | |
|---|---|
| [RealmListWeatherForecastdaySerializer](-realm-list-weather-forecastday-serializer.md) | [common]<br>constructor(dataSerializer: KSerializer&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;) |

## Properties

| Name | Summary |
|---|---|
| [descriptor](descriptor.md) | [common]<br>open override val [descriptor](descriptor.md): SerialDescriptor |

## Functions

| Name | Summary |
|---|---|
| [deserialize](deserialize.md) | [common]<br>open override fun [deserialize](deserialize.md)(decoder: Decoder): RealmList&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt; |
| [serialize](serialize.md) | [common]<br>open override fun [serialize](serialize.md)(encoder: Encoder, value: RealmList&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;) |
