//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherForecastdaySerializer](index.md)/[RealmListWeatherForecastdaySerializer](-realm-list-weather-forecastday-serializer.md)

# RealmListWeatherForecastdaySerializer

[common]\
constructor(dataSerializer: KSerializer&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;)
