//[weather_shared](../../index.md)/[multi.platform.weather.shared.external.realm](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [RealmListWeatherForecastdaySerializer](-realm-list-weather-forecastday-serializer/index.md) | [common]<br>class [RealmListWeatherForecastdaySerializer](-realm-list-weather-forecastday-serializer/index.md)(dataSerializer: KSerializer&lt;[WeatherForecastday](../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;) : KSerializer&lt;RealmList&lt;[WeatherForecastday](../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;&gt; |
| [RealmListWeatherSerializer](-realm-list-weather-serializer/index.md) | [common]<br>class [RealmListWeatherSerializer](-realm-list-weather-serializer/index.md)(dataSerializer: KSerializer&lt;[Weather](../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;) : KSerializer&lt;RealmList&lt;[Weather](../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;&gt; |
