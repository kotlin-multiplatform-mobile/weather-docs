//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherSerializer](index.md)/[RealmListWeatherSerializer](-realm-list-weather-serializer.md)

# RealmListWeatherSerializer

[common]\
constructor(dataSerializer: KSerializer&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;)
