//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherSerializer](index.md)/[serialize](serialize.md)

# serialize

[common]\
open override fun [serialize](serialize.md)(encoder: Encoder, value: RealmList&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;)
