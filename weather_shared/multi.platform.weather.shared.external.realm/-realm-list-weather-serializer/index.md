//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherSerializer](index.md)

# RealmListWeatherSerializer

[common]\
class [RealmListWeatherSerializer](index.md)(dataSerializer: KSerializer&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;) : KSerializer&lt;RealmList&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;&gt;

## Constructors

| | |
|---|---|
| [RealmListWeatherSerializer](-realm-list-weather-serializer.md) | [common]<br>constructor(dataSerializer: KSerializer&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;) |

## Properties

| Name | Summary |
|---|---|
| [descriptor](descriptor.md) | [common]<br>open override val [descriptor](descriptor.md): SerialDescriptor |

## Functions

| Name | Summary |
|---|---|
| [deserialize](deserialize.md) | [common]<br>open override fun [deserialize](deserialize.md)(decoder: Decoder): RealmList&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt; |
| [serialize](serialize.md) | [common]<br>open override fun [serialize](serialize.md)(encoder: Encoder, value: RealmList&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;) |
