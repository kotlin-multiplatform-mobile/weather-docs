//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external.realm](../index.md)/[RealmListWeatherSerializer](index.md)/[deserialize](deserialize.md)

# deserialize

[common]\
open override fun [deserialize](deserialize.md)(decoder: Decoder): RealmList&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;
