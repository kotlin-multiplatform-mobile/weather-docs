//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.currenttemp](../index.md)/[WeatherCurrentTempListener](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
abstract fun [onDataChanged](on-data-changed.md)(localTime: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), location: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), temp: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html))
