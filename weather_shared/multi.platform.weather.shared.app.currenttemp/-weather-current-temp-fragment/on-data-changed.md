//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.currenttemp](../index.md)/[WeatherCurrentTempFragment](index.md)/[onDataChanged](on-data-changed.md)

# onDataChanged

[android]\
open override fun [onDataChanged](on-data-changed.md)(localTime: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), location: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), temp: [Double](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-double/index.html))
