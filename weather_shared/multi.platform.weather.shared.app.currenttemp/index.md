//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.currenttemp](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherCurrentTempFragment](-weather-current-temp-fragment/index.md) | [android]<br>class [WeatherCurrentTempFragment](-weather-current-temp-fragment/index.md) : CoreFragment, [WeatherCurrentTempListener](-weather-current-temp-listener/index.md) |
| [WeatherCurrentTempListener](-weather-current-temp-listener/index.md) | [android]<br>interface [WeatherCurrentTempListener](-weather-current-temp-listener/index.md) |
