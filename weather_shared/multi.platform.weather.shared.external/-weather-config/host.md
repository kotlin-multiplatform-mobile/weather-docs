//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external](../index.md)/[WeatherConfig](index.md)/[host](host.md)

# host

[common]\
abstract val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
