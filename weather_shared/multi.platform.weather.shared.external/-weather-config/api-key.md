//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external](../index.md)/[WeatherConfig](index.md)/[apiKey](api-key.md)

# apiKey

[common]\
abstract val [apiKey](api-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
