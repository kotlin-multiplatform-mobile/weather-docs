//[weather_shared](../../../index.md)/[multi.platform.weather.shared.external](../index.md)/[WeatherConfig](index.md)

# WeatherConfig

[common]\
interface [WeatherConfig](index.md)

## Properties

| Name | Summary |
|---|---|
| [apiKey](api-key.md) | [common]<br>abstract val [apiKey](api-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [host](host.md) | [common]<br>abstract val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
