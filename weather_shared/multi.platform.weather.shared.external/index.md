//[weather_shared](../../index.md)/[multi.platform.weather.shared.external](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherConfig](-weather-config/index.md) | [common]<br>interface [WeatherConfig](-weather-config/index.md) |
