//[weather_shared](../../index.md)/[multi.platform.weather.shared.data.weather](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherRepositoryImpl](-weather-repository-impl/index.md) | [common]<br>class [WeatherRepositoryImpl](-weather-repository-impl/index.md)(realm: Realm, weatherConfig: [WeatherConfig](../multi.platform.weather.shared.external/-weather-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [WeatherRepository](../multi.platform.weather.shared.domain.weather/-weather-repository/index.md) |
