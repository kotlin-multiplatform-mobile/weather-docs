//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)

# WeatherRepositoryImpl

[common]\
class [WeatherRepositoryImpl](index.md)(realm: Realm, weatherConfig: [WeatherConfig](../../multi.platform.weather.shared.external/-weather-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) : [WeatherRepository](../../multi.platform.weather.shared.domain.weather/-weather-repository/index.md)

## Constructors

| | |
|---|---|
| [WeatherRepositoryImpl](-weather-repository-impl.md) | [common]<br>constructor(realm: Realm, weatherConfig: [WeatherConfig](../../multi.platform.weather.shared.external/-weather-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;) |

## Functions

| Name | Summary |
|---|---|
| [getWeatherForecastLocal](get-weather-forecast-local.md) | [common]<br>open suspend override fun [getWeatherForecastLocal](get-weather-forecast-local.md)(search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [getWeatherForecastNetwork](get-weather-forecast-network.md) | [common]<br>open suspend override fun [getWeatherForecastNetwork](get-weather-forecast-network.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [getWeatherLocationsLocal](get-weather-locations-local.md) | [common]<br>open suspend override fun [getWeatherLocationsLocal](get-weather-locations-local.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt; |
| [getWeatherLocationsNetwork](get-weather-locations-network.md) | [common]<br>open suspend override fun [getWeatherLocationsNetwork](get-weather-locations-network.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, order: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt; |
| [setWeatherForecastLocal](set-weather-forecast-local.md) | [common]<br>open suspend override fun [setWeatherForecastLocal](set-weather-forecast-local.md)(weatherForecast: [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [setWeatherLocationsLocal](set-weather-locations-local.md) | [common]<br>open suspend override fun [setWeatherLocationsLocal](set-weather-locations-local.md)(weatherLocations: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
