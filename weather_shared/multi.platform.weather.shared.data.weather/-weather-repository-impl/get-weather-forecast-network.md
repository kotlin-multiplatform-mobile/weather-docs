//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)/[getWeatherForecastNetwork](get-weather-forecast-network.md)

# getWeatherForecastNetwork

[common]\
open suspend override fun [getWeatherForecastNetwork](get-weather-forecast-network.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
