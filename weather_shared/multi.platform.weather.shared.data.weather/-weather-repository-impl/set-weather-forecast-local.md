//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)/[setWeatherForecastLocal](set-weather-forecast-local.md)

# setWeatherForecastLocal

[common]\
open suspend override fun [setWeatherForecastLocal](set-weather-forecast-local.md)(weatherForecast: [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
