//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)/[setWeatherLocationsLocal](set-weather-locations-local.md)

# setWeatherLocationsLocal

[common]\
open suspend override fun [setWeatherLocationsLocal](set-weather-locations-local.md)(weatherLocations: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
