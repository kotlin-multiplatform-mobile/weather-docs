//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)/[WeatherRepositoryImpl](-weather-repository-impl.md)

# WeatherRepositoryImpl

[common]\
constructor(realm: Realm, weatherConfig: [WeatherConfig](../../multi.platform.weather.shared.external/-weather-config/index.md), apiClientProvider: ApiClientProvider&lt;HttpClient&gt;)
