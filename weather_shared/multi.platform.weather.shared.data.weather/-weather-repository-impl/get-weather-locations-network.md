//[weather_shared](../../../index.md)/[multi.platform.weather.shared.data.weather](../index.md)/[WeatherRepositoryImpl](index.md)/[getWeatherLocationsNetwork](get-weather-locations-network.md)

# getWeatherLocationsNetwork

[common]\
open suspend override fun [getWeatherLocationsNetwork](get-weather-locations-network.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, order: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;
