//[weather_shared](../../index.md)/[multi.platform.weather.shared.app.common](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ItemWeatherForecastViewModel](-item-weather-forecast-view-model/index.md) | [common]<br>class [ItemWeatherForecastViewModel](-item-weather-forecast-view-model/index.md)(getUseCase: [GetWeatherForecastNetworkUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-network-use-case/index.md), readUseCase: [GetWeatherForecastLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-local-use-case/index.md), setUseCase: [SetWeatherForecastLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-set-weather-forecast-local-use-case/index.md)) : ItemViewModel&lt;[WeatherForecast](../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md), [GetWeatherForecastNetworkUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-network-use-case/index.md), [GetWeatherForecastLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-local-use-case/index.md), [SetWeatherForecastLocalUseCase](../multi.platform.weather.shared.domain.weather.usecase/-set-weather-forecast-local-use-case/index.md)&gt; |
