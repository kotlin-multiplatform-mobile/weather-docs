//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.common](../index.md)/[ItemWeatherForecastViewModel](index.md)/[ItemWeatherForecastViewModel](-item-weather-forecast-view-model.md)

# ItemWeatherForecastViewModel

[common]\
constructor(getUseCase: [GetWeatherForecastNetworkUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-network-use-case/index.md), readUseCase: [GetWeatherForecastLocalUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-get-weather-forecast-local-use-case/index.md), setUseCase: [SetWeatherForecastLocalUseCase](../../multi.platform.weather.shared.domain.weather.usecase/-set-weather-forecast-local-use-case/index.md))
