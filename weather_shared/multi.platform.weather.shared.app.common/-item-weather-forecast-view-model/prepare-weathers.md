//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.common](../index.md)/[ItemWeatherForecastViewModel](index.md)/[prepareWeathers](prepare-weathers.md)

# prepareWeathers

[common]\
fun [prepareWeathers](prepare-weathers.md)(weatherForecastdays: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;, currentEpoch: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) = Clock.System.now().epochSeconds): [HashMap](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-hash-map/index.html)&lt;[String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), [Any](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-any/index.html)&gt;
