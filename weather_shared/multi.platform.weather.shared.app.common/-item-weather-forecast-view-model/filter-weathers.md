//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.common](../index.md)/[ItemWeatherForecastViewModel](index.md)/[filterWeathers](filter-weathers.md)

# filterWeathers

[common]\
fun [filterWeathers](filter-weathers.md)(weatherForecastdays: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;, currentEpoch: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) = Clock.System.now().epochSeconds): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[Weather](../../multi.platform.weather.shared.domain.weather.entity/-weather/index.md)&gt;
