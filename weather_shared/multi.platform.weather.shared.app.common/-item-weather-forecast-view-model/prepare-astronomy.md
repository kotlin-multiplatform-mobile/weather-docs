//[weather_shared](../../../index.md)/[multi.platform.weather.shared.app.common](../index.md)/[ItemWeatherForecastViewModel](index.md)/[prepareAstronomy](prepare-astronomy.md)

# prepareAstronomy

[common]\
fun [prepareAstronomy](prepare-astronomy.md)(weatherForecastdays: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherForecastday](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecastday/index.md)&gt;, currentEpoch: [Long](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-long/index.html) = Clock.System.now().epochSeconds): [WeatherAstronomy](../../multi.platform.weather.shared.domain.weather.entity/-weather-astronomy/index.md)?
