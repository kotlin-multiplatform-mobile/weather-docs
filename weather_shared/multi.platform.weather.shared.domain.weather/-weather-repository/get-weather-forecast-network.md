//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)/[getWeatherForecastNetwork](get-weather-forecast-network.md)

# getWeatherForecastNetwork

[common]\
abstract suspend fun [getWeatherForecastNetwork](get-weather-forecast-network.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
