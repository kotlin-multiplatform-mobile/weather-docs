//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)

# WeatherRepository

interface [WeatherRepository](index.md)

#### Inheritors

| |
|---|
| [WeatherRepositoryImpl](../../multi.platform.weather.shared.data.weather/-weather-repository-impl/index.md) |

## Functions

| Name | Summary |
|---|---|
| [getWeatherForecastLocal](get-weather-forecast-local.md) | [common]<br>abstract suspend fun [getWeatherForecastLocal](get-weather-forecast-local.md)(search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [getWeatherForecastNetwork](get-weather-forecast-network.md) | [common]<br>abstract suspend fun [getWeatherForecastNetwork](get-weather-forecast-network.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [getWeatherLocationsLocal](get-weather-locations-local.md) | [common]<br>abstract suspend fun [getWeatherLocationsLocal](get-weather-locations-local.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt; |
| [getWeatherLocationsNetwork](get-weather-locations-network.md) | [common]<br>abstract suspend fun [getWeatherLocationsNetwork](get-weather-locations-network.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?, order: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt; |
| [setWeatherForecastLocal](set-weather-forecast-local.md) | [common]<br>abstract suspend fun [setWeatherForecastLocal](set-weather-forecast-local.md)(weatherForecast: [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md) |
| [setWeatherLocationsLocal](set-weather-locations-local.md) | [common]<br>abstract suspend fun [setWeatherLocationsLocal](set-weather-locations-local.md)(weatherLocations: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html) |
