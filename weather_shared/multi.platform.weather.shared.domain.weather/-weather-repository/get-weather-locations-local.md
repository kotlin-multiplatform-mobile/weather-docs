//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)/[getWeatherLocationsLocal](get-weather-locations-local.md)

# getWeatherLocationsLocal

[common]\
abstract suspend fun [getWeatherLocationsLocal](get-weather-locations-local.md)(offset: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), limit: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html), search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;
