//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)/[setWeatherLocationsLocal](set-weather-locations-local.md)

# setWeatherLocationsLocal

[common]\
abstract suspend fun [setWeatherLocationsLocal](set-weather-locations-local.md)(weatherLocations: [List](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-list/index.html)&lt;[WeatherLocation](../../multi.platform.weather.shared.domain.weather.entity/-weather-location/index.md)&gt;): [Boolean](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-boolean/index.html)
