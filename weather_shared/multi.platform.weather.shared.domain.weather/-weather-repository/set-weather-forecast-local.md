//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)/[setWeatherForecastLocal](set-weather-forecast-local.md)

# setWeatherForecastLocal

[common]\
abstract suspend fun [setWeatherForecastLocal](set-weather-forecast-local.md)(weatherForecast: [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
