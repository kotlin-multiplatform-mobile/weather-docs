//[weather_shared](../../../index.md)/[multi.platform.weather.shared.domain.weather](../index.md)/[WeatherRepository](index.md)/[getWeatherForecastLocal](get-weather-forecast-local.md)

# getWeatherForecastLocal

[common]\
abstract suspend fun [getWeatherForecastLocal](get-weather-forecast-local.md)(search: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?): [WeatherForecast](../../multi.platform.weather.shared.domain.weather.entity/-weather-forecast/index.md)
