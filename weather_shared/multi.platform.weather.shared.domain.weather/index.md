//[weather_shared](../../index.md)/[multi.platform.weather.shared.domain.weather](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [WeatherRepository](-weather-repository/index.md) | [common]<br>interface [WeatherRepository](-weather-repository/index.md) |
