//[example_android](../../../index.md)/[multi.platform.weather.example.app.home](../index.md)/[HomeFragment](index.md)/[onWeatherSearch](on-weather-search.md)

# onWeatherSearch

[androidJvm]\
open override fun [onWeatherSearch](on-weather-search.md)(query: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html))
