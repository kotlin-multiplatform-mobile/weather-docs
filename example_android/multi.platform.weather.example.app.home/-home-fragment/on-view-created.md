//[example_android](../../../index.md)/[multi.platform.weather.example.app.home](../index.md)/[HomeFragment](index.md)/[onViewCreated](on-view-created.md)

# onViewCreated

[androidJvm]\
open override fun [onViewCreated](on-view-created.md)(view: [View](https://developer.android.com/reference/kotlin/android/view/View.html), savedInstanceState: [Bundle](https://developer.android.com/reference/kotlin/android/os/Bundle.html)?)
