//[example_android](../../index.md)/[multi.platform.weather.example.app.home](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [HomeFragment](-home-fragment/index.md) | [androidJvm]<br>class [HomeFragment](-home-fragment/index.md) : CoreFragment, WeatherSearchbarListener |
