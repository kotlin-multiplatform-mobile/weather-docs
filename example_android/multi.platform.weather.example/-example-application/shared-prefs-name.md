//[example_android](../../../index.md)/[multi.platform.weather.example](../index.md)/[ExampleApplication](index.md)/[sharedPrefsName](shared-prefs-name.md)

# sharedPrefsName

[androidJvm]\
open override fun [sharedPrefsName](shared-prefs-name.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
