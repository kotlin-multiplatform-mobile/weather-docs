//[example_android](../../../index.md)/[multi.platform.weather.example](../index.md)/[ExampleApplication](index.md)/[appVersion](app-version.md)

# appVersion

[androidJvm]\
open override fun [appVersion](app-version.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
