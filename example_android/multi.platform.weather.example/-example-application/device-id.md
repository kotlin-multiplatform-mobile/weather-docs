//[example_android](../../../index.md)/[multi.platform.weather.example](../index.md)/[ExampleApplication](index.md)/[deviceId](device-id.md)

# deviceId

[androidJvm]\
open override fun [deviceId](device-id.md)(): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
