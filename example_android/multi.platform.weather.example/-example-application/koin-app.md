//[example_android](../../../index.md)/[multi.platform.weather.example](../index.md)/[ExampleApplication](index.md)/[koinApp](koin-app.md)

# koinApp

[androidJvm]\
open override val [koinApp](koin-app.md): (KoinApplication) -&gt; [Unit](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-unit/index.html)
