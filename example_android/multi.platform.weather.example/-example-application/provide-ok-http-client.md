//[example_android](../../../index.md)/[multi.platform.weather.example](../index.md)/[ExampleApplication](index.md)/[provideOkHttpClient](provide-ok-http-client.md)

# provideOkHttpClient

[androidJvm]\
open override fun [provideOkHttpClient](provide-ok-http-client.md)(x509TrustManager: [X509TrustManager](https://developer.android.com/reference/kotlin/javax/net/ssl/X509TrustManager.html)): OkHttpClient.Builder
