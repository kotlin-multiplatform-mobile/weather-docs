//[example_android](../../index.md)/[multi.platform.weather.example.app](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [MainActivity](-main-activity/index.md) | [androidJvm]<br>class [MainActivity](-main-activity/index.md) : CoreActivity |
