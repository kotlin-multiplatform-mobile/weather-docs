//[example_android](../../../index.md)/[multi.platform.weather.example.app](../index.md)/[MainActivity](index.md)/[nightMode](night-mode.md)

# nightMode

[androidJvm]\
open override fun [nightMode](night-mode.md)(): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
