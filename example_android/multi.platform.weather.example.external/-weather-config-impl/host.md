//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[WeatherConfigImpl](index.md)/[host](host.md)

# host

[androidJvm]\
open override val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
