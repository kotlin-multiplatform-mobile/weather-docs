//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[WeatherConfigImpl](index.md)

# WeatherConfigImpl

[androidJvm]\
class [WeatherConfigImpl](index.md)(val host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_SERVER, val apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_API_KEY) : WeatherConfig

## Constructors

| | |
|---|---|
| [WeatherConfigImpl](-weather-config-impl.md) | [androidJvm]<br>constructor(host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_SERVER, apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_API_KEY) |

## Properties

| Name | Summary |
|---|---|
| [apiKey](api-key.md) | [androidJvm]<br>open override val [apiKey](api-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [host](host.md) | [androidJvm]<br>open override val [host](host.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
