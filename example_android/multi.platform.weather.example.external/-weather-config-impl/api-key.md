//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[WeatherConfigImpl](index.md)/[apiKey](api-key.md)

# apiKey

[androidJvm]\
open override val [apiKey](api-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
