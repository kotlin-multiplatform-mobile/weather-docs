//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[WeatherConfigImpl](index.md)/[WeatherConfigImpl](-weather-config-impl.md)

# WeatherConfigImpl

[androidJvm]\
constructor(host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_SERVER, apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_API_KEY)
