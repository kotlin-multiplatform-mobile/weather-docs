//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[headerDeviceId](header-device-id.md)

# headerDeviceId

[androidJvm]\
open override val [headerDeviceId](header-device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
