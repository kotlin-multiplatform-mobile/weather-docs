//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[headerVersion](header-version.md)

# headerVersion

[androidJvm]\
open override val [headerVersion](header-version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
