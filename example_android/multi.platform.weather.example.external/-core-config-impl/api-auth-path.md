//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[apiAuthPath](api-auth-path.md)

# apiAuthPath

[androidJvm]\
open override val [apiAuthPath](api-auth-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null
