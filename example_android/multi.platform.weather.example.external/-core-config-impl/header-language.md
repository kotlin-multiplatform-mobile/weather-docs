//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[headerLanguage](header-language.md)

# headerLanguage

[androidJvm]\
open override val [headerLanguage](header-language.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
