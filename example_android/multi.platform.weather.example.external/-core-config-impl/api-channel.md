//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[apiChannel](api-channel.md)

# apiChannel

[androidJvm]\
open override val [apiChannel](api-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null
