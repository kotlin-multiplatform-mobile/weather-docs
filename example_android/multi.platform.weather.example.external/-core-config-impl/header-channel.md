//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)/[headerChannel](header-channel.md)

# headerChannel

[androidJvm]\
open override val [headerChannel](header-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?
