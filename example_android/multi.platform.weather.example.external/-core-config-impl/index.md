//[example_android](../../../index.md)/[multi.platform.weather.example.external](../index.md)/[CoreConfigImpl](index.md)

# CoreConfigImpl

[androidJvm]\
class [CoreConfigImpl](index.md)(val apiAuthPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val apiChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val apiRefreshTokenPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val headerChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-channel&quot;, val headerDeviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-device-id&quot;, val headerLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-language&quot;, val headerOs: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;, val headerVersion: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;) : CoreConfig

## Constructors

| | |
|---|---|
| [CoreConfigImpl](-core-config-impl.md) | [androidJvm]<br>constructor(apiAuthPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, apiChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, apiRefreshTokenPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, headerChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-channel&quot;, headerDeviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-device-id&quot;, headerLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-language&quot;, headerOs: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;, headerVersion: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;) |

## Properties

| Name | Summary |
|---|---|
| [apiAuthPath](api-auth-path.md) | [androidJvm]<br>open override val [apiAuthPath](api-auth-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null |
| [apiChannel](api-channel.md) | [androidJvm]<br>open override val [apiChannel](api-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null |
| [apiRefreshTokenPath](api-refresh-token-path.md) | [androidJvm]<br>open override val [apiRefreshTokenPath](api-refresh-token-path.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null |
| [headerChannel](header-channel.md) | [androidJvm]<br>open override val [headerChannel](header-channel.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerDeviceId](header-device-id.md) | [androidJvm]<br>open override val [headerDeviceId](header-device-id.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerLanguage](header-language.md) | [androidJvm]<br>open override val [headerLanguage](header-language.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerOs](header-os.md) | [androidJvm]<br>open override val [headerOs](header-os.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
| [headerVersion](header-version.md) | [androidJvm]<br>open override val [headerVersion](header-version.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? |
