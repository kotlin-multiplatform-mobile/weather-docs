//[example_android](../../index.md)/[multi.platform.weather.example.external](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [CoreConfigImpl](-core-config-impl/index.md) | [androidJvm]<br>class [CoreConfigImpl](-core-config-impl/index.md)(val apiAuthPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val apiChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val apiRefreshTokenPath: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = null, val headerChannel: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-channel&quot;, val headerDeviceId: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-device-id&quot;, val headerLanguage: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-language&quot;, val headerOs: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;, val headerVersion: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)? = &quot;x-header-version&quot;) : CoreConfig |
| [WeatherConfigImpl](-weather-config-impl/index.md) | [androidJvm]<br>class [WeatherConfigImpl](-weather-config-impl/index.md)(val host: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_SERVER, val apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) = BuildConfig.WEATHER_API_KEY) : WeatherConfig |
